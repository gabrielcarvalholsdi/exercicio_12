# exercicio_12
Utilizando a API do cartola e o projeto pronto, execute as tarefas a seguir. 

Informações sobre a API do cartola:

- https://api.cartolafc.globo.com/partidas
Traz as partidas da próxima rodada, além da lista de clubes e suas respectivas posições na tabela do Campeonato Brasileiro 2019 Série A.

- https://api.cartolafc.globo.com/partidas/<rodada>
Traz os resultados das partidas da rodada especificada. Também traz a posição daquele clube ao início dessa rodada.

- https://api.cartolafc.globo.com/atletas/mercado
Traz a lista de atletas de todos os clubes da Série A do Campeonato Brasileiro 2019.


## Tarefa 01

Utilizar o padrão BLOC para pegar a lista de clubes e exibir em uma ListView. 
OBS: O método que baixa o JSON e converte em uma lista de objetos “Clube” já está implementado

## Tarefa 02

Implementar uma lista com as partidas e o placar do jogo, de uma rodada escolhida pelo usuário. (Rodadas 1-5)